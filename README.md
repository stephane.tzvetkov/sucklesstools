# Suckless tools

This repository contains my own fork of some [suckless tools](https://suckless.org/).

Those forked tools are mostly the original tools with some patches I like. You will find the full
patches list in the `patch` folder of this repo. You can also find how I patched those tools in the
`docs` folder of this repo.

**Useful links**:

- <https://suckless.org/>
- <https://suckless.org/faq/>
- <https://dwm.suckless.org/>
- <https://github.com/LukeSmithxyz/dwm>
- <https://st.suckless.org/>
- <https://gitlab.com/LukeSmithxyz/st>
- <https://surf.suckless.org/>
- <https://tools.suckless.org/dmenu/>
- <https://gitlab.com/LukeSmithxyz/dmenu>
- <https://tools.suckless.org/slock/>
- <https://github.com/torrinfail/dwmblocks>
- <https://github.com/LukeSmithxyz/dwmblocks>
- <https://github.com/LukeSmithxyz/voidrice/tree/master/.local/bin/statusbar>
- <https://www.reddit.com/r/suckless/comments/g12r6z/pick_mix_your_patches_for_dwm_st_dmenu_slock_and/>

- <https://github.com/dunst-project/dunst>
- <https://dunst-project.org>
- <https://dunst-project.org/faq/>
- <https://dunst-project.org/documentation/>
- <https://wiki.archlinux.org/index.php/Dunst>
- <https://wiki.gentoo.org/wiki/Dunst>


---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [Install](#install)
* [Dependencies](#dependencies)
    * [dunst dependencies](#dunst-dependencies)
    * [st dependencies](#st-dependencies)
    * [dwm dependencies](#dwm-dependencies)
    * [st and dwm dependencies](#st-and-dwm-dependencies)
    * [dwmblocks dependencies](#dwmblocks-dependencies)
    * [slock dependencies](#slock-dependencies)
* [troubleshooting](#troubleshooting)
    * [slock troubleshooting](#slock-troubleshooting)
    * [dunst troubleshooting](#dunst-troubleshooting)

<!-- vim-markdown-toc -->


---
## Install

- If you want to use my fork of the suckless tools, first clone this repo:
    ```console
    $ git clone https://gitlab.com/stephane.tzvetkov/sucklesstools.git
    $ cd sucklesstools
    ```

    Then, e.g. if you want my fork of `dwm`: **first, check the related
    [`dependencies`](#dependencies)**, and then run the following commands:
    ```console
    $ cd dwm-x.y # replace `x.y` by the version of dwm used in my repo, e.g. `dwm-6.2`
    $ vi config.h # you also might want to change some configs according to your preferences
        > ...
    $ make clean
    $ make # or `$ sudo make install` if you want to install it system-wide
    ```

- If you want to build yourself some tools, from scratch, then you might be interrested in:
    - my [`cheatsheet about suckless
      tools`](https://stephane-cheatsheets.readthedocs.io/en/latest/graphics/suckless_tools/): how
      to download, install and patch them from scratch.
    - my [`patch process`](./docs) documentation: how did I patch my suckless tools.

---
## Dependencies

### dunst dependencies

- libnotify (for dunstify)
- libxdg-basedir
- libxinerama (usualy comes with mpv, or vlc)
- libxrandr (usualy comes with mesa, or xrandr or mpv)
- pango (usualy comes with firefox)
    - cairo (usualy comes with pango)
    - ?libxft (?usualy comes with pango?)
    - ?libxss (?usualy comes with pango?)
- libgtk3.0 (usualy comes with `gtk3` which comes with `gnome-desktop` based GUI apps)
- dbus (usualy comes with xorg)
    - glib (usualy comes with dbus)

- A proper icons pack should be insalled (see my [icons
  cheatsheets](https://stephane-cheatsheets.readthedocs.io/en/latest/graphical/icons/) if you don't
  know how to). Also, the `dunstrc` file should be configured accordingly (see my
  [dunstrc](./dunst-1.5.0/dunstrc) as an example), in particular the `icon_path` option **and** the
  `icon` option**s** (by default, the `arc-icon-theme` is expected on Arch Linux).

- Don't forget to remove or disable any pre-existing notification manager (e.g.
  `notification-daemon`).

- Also, after building `dunst`, create a `dunstrc` configuration file like so:
  ```console
  $ mkdir ~/.config/dunst
  $ ln -s /path/to/this/repo/dunst-x.y.z/dunstrc ~/.config/dunst/
  ```

### st dependencies

- [`harfbuzz`](https://repology.org/project/harfbuzz/versions) will be needed for the [ligature
  patch](https://st.suckless.org/patches/ligatures/), e.g. with pacman:
  ```console
  # pacman -S harfbuzz
  ```

- optionnal: `dmenu` with the `dmenu-alpha` patch will be needed if you want to use the
  [`iso14755`](https://st.suckless.org/patches/iso14755/) patch (see [dmenu patch
  process](./dmenu-patch-process.md) for more details) with `st` (note that the `dmenu-apha` patch
  will be only neede if `st` has been patch with the `st-alpha` patch).

> **Don't forget to also refer to [st and dwm dependencies](#st-and-dwm-dependencies)**

### dwm dependencies

- `lixcb`, `Xlib-libxcb` and `xcb-res` (which usualy come with `libxinerama` or `xorg`?) will be
  needed for the [`swallow`](https://dwm.suckless.org/patches/swallow/) patch needs those
  dependencies:
    - `libxcb`
    - `Xlib-libxcb`
    - `xcb-res`

> **Don't forget to also refer to [st and dwm dependencies](#st-and-dwm-dependencies)**

### st and dwm dependencies

- `Xcompmgr`

    - `Xcompmgr` will be needed for the [st alpha patch](https://st.suckless.org/patches/alpha/)
      and the [dwm alpha patch](https://dwm.suckless.org/patches/alpha/). But before installing it,
      make sure you have installed and correctly configured `xorg`. To make sure the `Composite`
      extension is enabled for the X Server, run:
        ```console
        $ xdpyinfo | grep Composite
          > Composite
        ```

    - If the last command had no output, add the `Composite` option to the `Extensions` section of
      `xorg.conf`:
        ```console
        # vi /etc/X11/xorg.conf
            > ...
            > Section "Extensions"
            >         Option  "Composite" "true"
            > EndSection
            > ...
        ```

    - Install [xcompmgr](https://repology.org/project/xcompmgr/versions), e.g. with pacman:
        ```console
        # pacman -S xcompmgr
        ```

    - Now you can run it like so (and eventually add it to your `.xinitrc`):
        ```console
        $ xcompmgr -c -o 0.2 & # -c: client-side compositing with translucency support
                               # -o 0.2: specifies 20% opacity for client-side shadows
        ```

- fonts
    - See [my fonts cheatsheets](https://stephane-cheatsheets.readthedocs.io/en/latest/misc/fonts/)
      if needed.
    - Download the Hack Nerd Font here: https://www.nerdfonts.com/font-downloads (or from package
      manager if available)
    - Download the Noto Fonts Emoji here: https://github.com/googlefonts/noto-emoji (or from
      package manager if available)
    - Download the Symbola Font: https://dn-works.com/ufas/ (or from package manager if available,
      also available as aur package: https://aur.archlinux.org/packages/ttf-symbola/)
    - Install the [libXft](https://gitlab.freedesktop.org/xorg/lib/libxft) [BGRA patch](https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1)
      in order for `st` and `dwm` to render emojis.

        - On Arch Linux:

            Just install the [libxft-bgra aur
            package](https://aur.archlinux.org/packages/libxft-bgra/).

            Do not hesitate to replace libxft:
            ```console
            :: libxft-bgra and libxft are in conflict. Remove libxft? [y/N] y
            ```

        - On Gentoo:

            Just recompile the `libXft-2.3.4` package with the BGRA patch:
            ```console
            # echo "x11-libs/libXft ~amd64" >> /etc/portage/package.accept_keywords
            # mkdir -p /etc/portage/patches/x11-libs/libXft-2.3.4
            $ cd /etc/portage/patches/x11-libs/libXft-2.3.4
            # wget https://gitlab.freedesktop.org/xorg/lib/libxft/-/merge_requests/1.patch
            # emerge -a1 x11-libs/libXft
            ```

### dwmblocks dependencies

- the [dwmblocks-widgets](./dwmblocks-widgets) scripts assciated with my build of `dwmblocks`
  (located in the `dwmblocks-widgets` folder of this repo). Those scripts needs to be in your
  `$PATH`.

- `dunst` (in order use `notify-send` on events)

- in order for widgets to be clickable...
    - ... it requires a patched `dwm` with the
      [statuscmd-signal](https://dwm.suckless.org/patches/statuscmd/dwm-statuscmd-signal-6.2.diff)
      patch (see https://dwm.suckless.org/patches/statuscmd/). You can refer to [the `dwm` patch
      process](./docs/dwm-patch-process.md) for more details.
    - ... it requires a patched `dwmblocks` with the
      [dwmblocks-statuscmd](https://gist.github.com/toniz4/41d168719e22bf7bc4ecff09d424b7d2) patch
      (see https://github.com/torrinfail/dwmblocks#patches). You can refer to [the `dwmblocks`
      patch process](./docs/dwmblocks-patch-process.md) for more details.

- optionnal: `ikhal` for the clock widget when clicking it.

- optionnal: `todoman` for the clock widget when clicking it.

- optionnal: `acpi` for the battery widget when clicking it.

- ⚠️ **DEPRECATED**⚠️ `lm_sensors` ⚠️ **DEPRECATED**⚠️
    - Make sure [`lm_sensors`](https://repology.org/project/lm-sensors/versions) is installed for
      the [`dwmblocks-cpu-temp`](./dwmblocks-widgets/dwmblocks-cpu-temp) widget.

- `bc`
    - Make sure [`bc`](https://repology.org/project/bc/versions) is installed for the
      [`dwmblocks-cpu-temp`](./dwmblocks-widgets/dwmblocks-cpu-temp) and the
      `dwmblocks-last-update` widgets.

- `mpstat`
    - Make sure `mpstat`, which is part of
      [`sysstat`](https://repology.org/project/sysstat/versions) is installed for the
      [`dwmblocks-cpu-bars`](./dwmblocks-widgets/dwmblocks-cpu-bars) widget.

- `htop`
    - Make sure [`htop`](https://repology.org/project/htop/versions), is installed for the
      [`dwmblocks-cpu-bars`](./dwmblocks-widgets/dwmblocks-cpu-bars) widget.

- `redshift`
    - Make sure [`redshift`](https://repology.org/project/redshift/versions) is installed for the
      [`dwmblocks-redshift-temp`](./dwmblocks-widgets/dwmblocks-redshift-temp) widget.

- `xbacklight`
    - Make sure [`xbacklight`](https://repology.org/project/xbacklight/versions) is installed for the
      [`dwmblocks-brightness`](./dwmblocks-widgets/dwmblocks-brightness) widget.


### slock dependencies

- See <https://git.archlinux.org/svntogit/community.git/tree/trunk/PKGBUILD?h=packages/slock>

- `libXext` and `libxrandr`
    - Make sure that [`libXext`](https://repology.org/project/libxext/versions) and
      [`libXrandr`](https://repology.org/project/libxrandr/versions) are installed.

- `imlib2`
    - Make sure that [`imlib2`](https://repology.org/project/imlib2/versions) is installed (usualy
      comes with feh).


---
## troubleshooting

### slock troubleshooting

- If you get this error `slock: getgrnam nogroup: group entry not found`, then you might want to
  apply this:
  ```console
  $ sed -i 's|static const char \*group = "nogroup";|static const char *group = "nobody";|' config.def.h
  $ sed -ri 's/((CPP|C|LD)FLAGS) =/\1 +=/g' config.mk
  ```
  Then don't forget to change `config.h` according to the new `config.def.h`.

- If you get this error `slock: unable to disable OOM killer. Make sure to suid or sgid slock.`,
  then you might want to build `slock` like so: `$ make clean && sudo make install` to install it
  system-wide and not just user-wide.


### dunst troubleshooting

> **Reference**: https://dunst-project.org/faq/

- If you get a similar error: `Cannot acquire 'org.freedesktop.Notifications'...`, then you might
  have a notification manager already running. Remove or disable it.

  E.g. with `notification-daemon` on Gentoo:
    ```console
    $ sudo killall notification-manager
    $ sudo emerge --ask --verbose --depclean x11-misc/notification-daemon
    ```

- If you get the following error: `WARNING: No dunstrc found.`, then a configuration might be
  needed like so:
    ```console
    $ mkdir ~/.config/dunst
    $ ln -s /path/to/this/repo/dunst-x.y.z/dunstrc ~/.config/dunst/
    ```

