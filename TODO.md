# TODO

---
## Table of contents

<!-- vim-markdown-toc GitLab -->

* [suckless TODO](#suckless-todo)
* [dunst TODO](#dunst-todo)
* [dwm TODO](#dwm-todo)
* [dwmblocks TODO](#dwmblocks-todo)
* [st TODO](#st-todo)
* [dmenu TODO](#dmenu-todo)

<!-- vim-markdown-toc -->


---
## suckless TODO

- `tabbed`: <https://www.youtube.com/watch?v=-Xr4ouz8zQA>
- `sxiv`: <https://www.youtube.com/watch?v=IUvM2Hc6jtk>
- `surf`: <https://surf.suckless.org/>


---
## dunst TODO


---
## dwm TODO

- move dwm-scripts into scripts repo
- cyclelayouts patch: <https://dwm.suckless.org/patches/cyclelayouts/>
- bottomstack patch: <https://dwm.suckless.org/patches/bottomstack/>
- fibonacci patch: <https://dwm.suckless.org/patches/fibonacci/>
- gridmode patch: <https://dwm.suckless.org/patches/gridmode/>
- ? vanitygaps patch: <https://dwm.suckless.org/patches/vanitygaps/>

- ? statuscolor patch: <https://dwm.suckless.org/patches/statuscolors/>
- ? colorbar patch: <https://dwm.suckless.org/patches/colorbar/>

- ? more patches to add: <https://github.com/lukesmithxyz/dwm#patches-and-features>

- ? bar_height patch: <https://dwm.suckless.org/patches/bar_height/>


---
## dwmblocks TODO

- add audio widget!
- merge IP and internet widgets
- add wireless quality on click
- add todo widget ? run todoman when clicking it
- add iftop or nethog (and netstat?) on click on net traffic widget?
- fix wetter data collection rate (force data collect on new internet connexion)
- fix the widget that slows down the whole bar if no internet connexion
- dwm status bar:
    - <https://dwm.suckless.org/status_monitor/>
    - <https://github.com/torrinfail/dwmblocks>
    - <https://github.com/LukeSmithxyz/dwmblocks>
    - <https://github.com/LukeSmithxyz/voidrice/tree/master/.local/bin/statusbar>
    - <https://www.youtube.com/watch?v=UP2QpHmcgyk>
- <https://www.youtube.com/watch?v=w5phSVBEHhQ>
- clickable weather widget with wttr report + <https://www.youtube.com/watch?v=qNtjud8zNa0>
- audio widget
- test mail widget
- ? ncmcpp widget
- ? mpd widget
- ? ihkal widget
- ? todoman widget
- xbacklight inc on battery widget button1 or button4 (mouse wheel up) or on screen widget ?
- xbacklight dec on battery widget button3 or button5 (mouse wheel down ) or on screen widget ?


---
## st TODO

- check 🔥 https://github.com/siduck/st 🔥
- change `keyboard-select` shortcuts

---
## dmenu TODO

- dmenu script to insert emojis
- dmenu scripts to handle usb, wifi and ethernet connexion
- <https://github.com/march-linux/connman_dmenu>

- <https://www.youtube.com/watch?v=vrN5SPeVUu0>
- <https://github.com/c-14/dmenu-scripts>
- <https://github.com/glotzbam/dmenu_wpa_cli>
- dmenu ethernet and wifi
- dmenu bluetooth
- dmenu usb
- dmenu mount / unmount
- dmenu emoji selector: <https://github.com/lyneca/emojenu>
- <https://github.com/topics/dmenu-scripts>
- <https://github.com/danielleontiev/suckless-yt>
- <https://github.com/danielleontiev/suckless-yt>

