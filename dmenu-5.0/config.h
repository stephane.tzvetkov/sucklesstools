/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

/* colors */
static const char col_gray1[]        = "#222222";
static const char col_gray2[]        = "#444444";
static const char col_gray3[]        = "#bbbbbb";
static const char col_gray4[]        = "#eeeeee";

static const char col_white[]        = "#ffffff";
static const char col_black[]        = "#000000";

static const char col_red[]          = "#ff0000"; /* x11 red */
static const char col_orangered[]    = "#ff5f00"; /* x11 orange red 1 */
static const char col_orange[]       = "#ffa500"; /* x11 orange */
static const char col_gold[]         = "#ffd700"; /* x11 gold */
static const char col_yellow[]       = "#ffff00"; /* x11 yellow */
static const char col_greenyellow[]  = "#adff2f"; /* x11 green yellow */
static const char col_limegreen[]    = "#32cd32"; /* x11 lime green */
static const char col_springgreen[]  = "#00ff7f"; /* x11 spring green */
static const char col_turquoise[]    = "#40e0d0"; /* x11 turquoise */
static const char col_cyan[]         = "#00FFFF"; /* x11 cyan */
static const char col_deepskyblue[]  = "#00BFFF"; /* x11 deep sky blue */
static const char col_royalblue[]    = "#4169e1"; /* x11 royal blue */
static const char col_navyblue[]     = "#000080"; /* x11 navy blue */
static const char col_midnightblue[] = "#191970"; /* x11 midnight blue */
static const char col_blueviolet[]   = "#8a2be2"; /* x11 blue violet */
static const char col_violetred[]    = "#ee3a8c"; /* x11 violet red */
static const char col_magenta[]      = "#ff00ff"; /* x11 magenta */
static const char col_hotpink[]      = "#ff69b4"; /* x11 hot pink */
static const char col_darkred[]      = "#8b0000"; /* x11 dark red */
static const char col_ceared[]       = "#ba0c1a"; /* CEA red */

static int topbar = 1;      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;       /* -F  option; if 0, dmenu doesn't use fuzzy matching     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
    "Hack Nerd Font:pixelsize=16:antialias=true:autohint=true",
    "NotoColorEmoji:pixelsize=16:antialias=true:autohint=true"
    "Symbola:pixelsize=16:antialias=true:autohint=true",
    "Inconsolata:pixelsize=16:antialias=true:autohint=true",
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
    /*                      fg              bg       */
    /*
    // red variant
    [SchemeNorm] =          { col_ceared,   col_black },
    [SchemeSel] =           { col_white,    col_black },
    [SchemeSelHighlight] =  { col_gold,     col_ceared },
    [SchemeNormHighlight] = { col_gold,     col_darkred },
    [SchemeOut] =           { "#000000",    "#00ffff" },
    */
    /*
    // blue variant
    [SchemeNorm] =          { col_royalblue, col_black },
    [SchemeSel] =           { col_white,     col_black },
    [SchemeSelHighlight] =  { col_gold,      col_royalblue },
    [SchemeNormHighlight] = { col_gold,      col_midnightblue },
    [SchemeOut] =           { "#000000",     "#00ffff" },
    */
    // white variant
    [SchemeNorm] =          { col_white,     col_black },
    [SchemeSel] =           { col_white,     col_black },
    [SchemeSelHighlight] =  { col_gold,      col_black },
    [SchemeNormHighlight] = { col_orangered, col_black },
    [SchemeOut] =           { col_black,     col_black },
};

static const unsigned int alpha = 0xf0;
static const unsigned int alphas[SchemeLast][2] = {
	[SchemeNorm] = { OPAQUE, alpha },
	[SchemeSel] = { OPAQUE, alpha },
	[SchemeOut] = { OPAQUE, alpha },
};

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
