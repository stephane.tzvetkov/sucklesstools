# dmenu patch process

Here is the full patch process I applied to `dmenu`:

* Apply the [`fuzzymatch`](https://tools.suckless.org/dmenu/patches/fuzzymatch/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://tools.suckless.org/dmenu/patches/fuzzymatch/dmenu-fuzzymatch-4.9.diff > dmenu-fuzzymatch-4.9.diff
$ cd ../dmenu-x.x
$ patch -p1 < ../patches/dmenu-fuzzymatch-4.9.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

* Apply the [`fuzzyhighlight`](https://tools.suckless.org/dmenu/patches/fuzzyhighlight/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://tools.suckless.org/dmenu/patches/fuzzyhighlight/dmenu-fuzzyhighlight-4.9.diff > dmenu-fuzzyhighlight-4.9.diff
$ cd ../dmenu-x.x
$ patch -p1 < ../patches/dmenu-fuzzyhighlight-4.9.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the [`numbers`](https://tools.suckless.org/dmenu/patches/numbers/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://tools.suckless.org/dmenu/patches/numbers/dmenu-numbers-4.9.diff > dmenu-numbers-4.9.diff
$ cd ../dmenu-x.x
$ patch -p1 < ../patches/dmenu-numbers-4.9.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
* Apply the [`alpha`]() patch:
```console
$ cd /path/to/this/repo/patches
$ curl TODO-add-link > dmenu-alpha-patch-for-embedded-in-transparent-st.diff
```

For the patch to work with all the previous patches, you will have to modify it.
```console
$ cp dmenu-alpha-patch-for-embedded-in-transparent-st.diff dmenu-alpha-patch-for-embedded-in-transparent-st-perso.diff
$ vi dmenu-alpha-patch-for-embedded-in-transparent-st-perso.diff
```

Replace this hunk:
```console
@@ -20,7 +20,7 @@ FREETYPEINC = /usr/include/freetype2
 
 # includes and libs
 INCS = -I$(X11INC) -I$(FREETYPEINC)
-LIBS = -L$(X11LIB) -lX11 $(XINERAMALIBS) $(FREETYPELIBS)
+LIBS = -L$(X11LIB) -lX11 $(XINERAMALIBS) $(FREETYPELIBS) -lXrender
 
 # flags
 CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_XOPEN_SOURCE=700 -D_POSIX_C_SOURCE=200809L -DVERSION=\"$(VERSION)\" $(XINERAMAFLAGS)
```

By this hunk:
```console
@@ -20,7 +20,7 @@ FREETYPEINC = /usr/include/freetype2
 
 # includes and libs
 INCS = -I$(X11INC) -I$(FREETYPEINC)
-LIBS = -L$(X11LIB) -lX11 $(XINERAMALIBS) $(FREETYPELIBS) -lm
+LIBS = -L$(X11LIB) -lX11 $(XINERAMALIBS) $(FREETYPELIBS) -lm -lXrender
 
 # flags
 CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_XOPEN_SOURCE=700 -D_POSIX_C_SOURCE=200809L -DVERSION=\"$(VERSION)\" $(XINERAMAFLAGS)
```

Then:
```console
$ cd ../dmenu-x.x
$ patch -p1 < ../patches/dmenu-alpha-patch-for-embedded-in-transparent-st-perso.diff
$ make clean && make
```
