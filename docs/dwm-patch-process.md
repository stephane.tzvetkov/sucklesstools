# dwm patch process

Here is the full patch process I applied to `dwm`:

* Apply the [`alpha`](https://dwm.suckless.org/patches/alpha/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/alpha/dwm-alpha-20180613-b69c870.diff > dwm-alpha-20180613-b69c870.diff
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-alpha-20180613-b69c870.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

* Apply the [`runtime useless gaps`](https://dwm.suckless.org/patches/ru_gaps/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/ru_gaps/dwm-ru_gaps-6.2.diff > dwm-ru_gaps-6.2.diff
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-ru_gaps-6.2.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

* Apply the [`notitle`](https://dwm.suckless.org/patches/notitle/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/notitle/dwm-notitle-6.2.diff > dwm-notitle-6.2.diff
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-ru_gaps-6.2.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

* Apply the [`clientindicators`](https://dwm.suckless.org/patches/clientindicators/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/clientindicators/dwm-clientindicatorshidevacant-6.2.diff > dwm-clientindicatorshidevacant-6.2.diff
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-ru_gaps-6.2.diff
$ make clean && make
```

* Apply the [`attachdirection`](https://dwm.suckless.org/patches/attachdirection/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/attachdirection/dwm-attachdirection-6.2.diff > dwm-attachdirection-6.2.diff
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-ru_gaps-6.2.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

* Apply the [`shiftview`](https://github.com/LukeSmithxyz/dwm/search?q=shiftview) patch:
```console
$ cd /path/to/this/repo/dwm-6.2

$ nvim config.h
    > ...
  + > #include "shiftview.c"
  + >
    >  static Key keys[] = {
    >         /* modifier                     key        function        argument */
    > ...
  - >         { MODKEY,                       XK_Tab,    view,           {0} },
  + > //      { MODKEY,                       XK_Tab,    view,           {0} },
  + >         { MODKEY,                       0xb2,      view,           {0} }, /* ² */
    > ...
  + >         { MODKEY,                       XK_Tab,    shiftview,      { .i = +1 } },
  + >         { MODKEY|ShiftMask,             XK_Tab,    shiftview,      { .i = -1 } },
    > ...

$ nvim shiftview.c
    > /** Function to shift the current view to the left/right
    >  *
    >  * @param: "arg->i" stores the number of tags to shift right (positive value)
    >  *          or left (negative value)
    >  */
    > void
    > shiftview(const Arg *arg)
    > {
    >   Arg a;
    >   Client *c;
    >   unsigned visible = 0;
    >   int i = arg->i;
    >   int count = 0;
    >   int nextseltags, curseltags = selmon->tagset[selmon->seltags];
    >
    >   do {
    >       if(i > 0) // left circular shift
    >           nextseltags = (curseltags << i) | (curseltags >> (LENGTH(tags) - i));
    >
    >       else // right circular shift
    >           nextseltags = curseltags >> (- i) | (curseltags << (LENGTH(tags) + i));
    >
    >                 // Check if tag is visible
    >       for (c = selmon->clients; c && !visible; c = c->next)
    >           if (nextseltags & c->tags) {
    >               visible = 1;
    >               break;
    >           }
    >       i += arg->i;
    >   } while (!visible && ++count < 10);
    >
    >   if (count < 10) {
    >       a.i = nextseltags;
    >       view(&a);
    >   }
    > }
    >
    > void
    > shifttag(const Arg *arg)
    > {
    >   Arg a;
    >   Client *c;
    >   unsigned visible = 0;
    >   int i = arg->i;
    >   int count = 0;
    >   int nextseltags, curseltags = selmon->tagset[selmon->seltags];
    >
    >   do {
    >       if(i > 0) // left circular shift
    >           nextseltags = (curseltags << i) | (curseltags >> (LENGTH(tags) - i));
    >
    >       else // right circular shift
    >           nextseltags = curseltags >> (- i) | (curseltags << (LENGTH(tags) + i));
    >
    >                 // Check if tag is visible
    >       for (c = selmon->clients; c && !visible; c = c->next)
    >           if (nextseltags & c->tags) {
    >               visible = 1;
    >               break;
    >           }
    >       i += arg->i;
    >   } while (!visible && ++count < 10);
    >
    >   if (count < 10) {
    >       a.i = nextseltags;
    >       tag(&a);
    >   }
    > }
```

* /!\ WIP: NOT WORKING YET /!\ Apply the [`stacker`](https://dwm.suckless.org/patches/stacker/)
  patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/stacker/dwm-stacker-6.2.diff > dwm-stacker-6.2.diff
```
For the patch to work with all the previous patches, you will have to modify it.
```console
$ cp dwm-stacker-6.2.diff dwm-stacker-perso-6.2.diff
$ vi dwm-stacker-perso-6.2.diff
```

Replace this part:
```console
--- a/dwm.c
+++ b/dwm.c
@@ -47,15 +47,21 @@
 /* macros */
 #define BUTTONMASK              (ButtonPressMask|ButtonReleaseMask)
 #define CLEANMASK(mask)         (mask & ~(numlockmask|LockMask) & (ShiftMask|ControlMask|Mod1Mask|Mod2Mask|Mod3Mask|Mod4Mask|Mod5Mask))
+#define GETINC(X)               ((X) - 2000)
+#define INC(X)                  ((X) + 2000)
 #define INTERSECT(x,y,w,h,m)    (MAX(0, MIN((x)+(w),(m)->wx+(m)->ww) - MAX((x),(m)->wx)) \
                                * MAX(0, MIN((y)+(h),(m)->wy+(m)->wh) - MAX((y),(m)->wy)))
+#define ISINC(X)                ((X) > 1000 && (X) < 3000)
 #define ISVISIBLE(C)            ((C->tags & C->mon->tagset[C->mon->seltags]))
+#define PREVSEL                 3000
 #define LENGTH(X)               (sizeof X / sizeof X[0])
+#define MOD(N,M)                ((N)%(M) < 0 ? (N)%(M) + (M) : (N)%(M))
 #define MOUSEMASK               (BUTTONMASK|PointerMotionMask)
 #define WIDTH(X)                ((X)->w + 2 * (X)->bw)
 #define HEIGHT(X)               ((X)->h + 2 * (X)->bw)
 #define TAGMASK                 ((1 << LENGTH(tags)) - 1)
 #define TEXTW(X)                (drw_fontset_getwidth(drw, (X)) + lrpad)
+#define TRUNC(X,A,B)            (MAX((A), MIN((X), (B))))

 /* enums */
 enum { CurNormal, CurResize, CurMove, CurLast }; /* cursor */
```

By this part:
```console
--- a/dwm.c
+++ b/dwm.c
@@ -47,16 +47,22 @@
 /* macros */
 #define BUTTONMASK              (ButtonPressMask|ButtonReleaseMask)
 #define CLEANMASK(mask)         (mask & ~(numlockmask|LockMask) & (ShiftMask|ControlMask|Mod1Mask|Mod2Mask|Mod3Mask|Mod4Mask|Mod5Mask))
+#define GETINC(X)               ((X) - 2000)
+#define INC(X)                  ((X) + 2000)
 #define INTERSECT(x,y,w,h,m)    (MAX(0, MIN((x)+(w),(m)->wx+(m)->ww) - MAX((x),(m)->wx)) \
                                * MAX(0, MIN((y)+(h),(m)->wy+(m)->wh) - MAX((y),(m)->wy)))
+#define ISINC(X)                ((X) > 1000 && (X) < 3000)
 #define ISVISIBLEONTAG(C, T)    ((C->tags & T))
 #define ISVISIBLE(C)            ISVISIBLEONTAG(C, C->mon->tagset[C->mon->seltags])
+#define PREVSEL                 3000
 #define LENGTH(X)               (sizeof X / sizeof X[0])
+#define MOD(N,M)                ((N)%(M) < 0 ? (N)%(M) + (M) : (N)%(M))
 #define MOUSEMASK               (BUTTONMASK|PointerMotionMask)
 #define WIDTH(X)                ((X)->w + 2 * (X)->bw)
 #define HEIGHT(X)               ((X)->h + 2 * (X)->bw)
 #define TAGMASK                 ((1 << LENGTH(tags)) - 1)
 #define TEXTW(X)                (drw_fontset_getwidth(drw, (X)) + lrpad)
+#define TRUNC(X,A,B)            (MAX((A), MIN((X), (B))))

 #define OPAQUE                  0xffU

```

Then:
```console
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-ru_gaps-6.2.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the [`movestack`](https://dwm.suckless.org/patches/movestack/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/movestack/dwm-movestack-6.1.diff > dwm-movestack-6.1.diff
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-movestack-6.1.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the [`sticky`](https://dwm.suckless.org/patches/sticky/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/sticky/dwm-sticky-6.1.diff > dwm-sticky-6.1.diff
```

For the patch to work with all the previous patches, you will have to modify it.
```console
$ cp dwm-sticky-6.1.diff dwm-sticky-perso-6.1.diff
$ vi dwm-sticky-perso-6.1.diff
```

Replace this part:
```console
@@ -49,7 +49,7 @@
 #define CLEANMASK(mask)         (mask & ~(numlockmask|LockMask) & (ShiftMask|ControlMask|Mod1Mask|Mod2Mask|Mod3Mask|Mod4Mask|Mod5Mask))
 #define INTERSECT(x,y,w,h,m)    (MAX(0, MIN((x)+(w),(m)->wx+(m)->ww) - MAX((x),(m)->wx)) \
                                * MAX(0, MIN((y)+(h),(m)->wy+(m)->wh) - MAX((y),(m)->wy)))
-#define ISVISIBLE(C)            ((C->tags & C->mon->tagset[C->mon->seltags]))
+#define ISVISIBLE(C)            ((C->tags & C->mon->tagset[C->mon->seltags]) || C->issticky)
 #define LENGTH(X)               (sizeof X / sizeof X[0])
 #define MOUSEMASK               (BUTTONMASK|PointerMotionMask)
 #define WIDTH(X)                ((X)->w + 2 * (X)->bw)
```

By this part:
```console
@@ -49,7 +49,7 @@
 #define INTERSECT(x,y,w,h,m)    (MAX(0, MIN((x)+(w),(m)->wx+(m)->ww) - MAX((x),(m)->wx)) \
                                * MAX(0, MIN((y)+(h),(m)->wy+(m)->wh) - MAX((y),(m)->wy)))
 #define ISVISIBLEONTAG(C, T)    ((C->tags & T))
-#define ISVISIBLE(C)            ISVISIBLEONTAG(C, C->mon->tagset[C->mon->seltags])
+#define ISVISIBLE(C)            ISVISIBLEONTAG(C, C->mon->tagset[C->mon->seltags] || C->issticky)
 #define LENGTH(X)               (sizeof X / sizeof X[0])
 #define MOUSEMASK               (BUTTONMASK|PointerMotionMask)
 #define WIDTH(X)                ((X)->w + 2 * (X)->bw)
```

```console
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-sticky-perso-6.1.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the [`monoclesymbol`](https://dwm.suckless.org/patches/monoclesymbol/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/monoclesymbol/dwm-monoclesymbol-6.2.diff > dwm-monoclesymbol-6.2.diff

$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-monoclesymbol-6.2.diff
$ make clean && make
```

* Apply the [`statuscmd-signal`](https://dwm.suckless.org/patches/statuscmd/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/statuscmd/dwm-statuscmd-signal-6.2.diff > dwm-statuscmd-signal-6.2.diff
```

> Note that this patch is intended to enable the `dwmblocks` clickable features (cf. [dwmblocks
> dependencies](../README.md#dwmblocks-dependencies), and [dwmblocks patch
> process](./dwmblocks-patch-process.md) for more details).

For the patch to work with all the previous patches, you will have to modify it.
```console
$ cp dwm-statuscmd-signal-6.2.diff dwm-statuscmd-signal-perso-6.2.diff
$ vi dwm-statuscmd-signal-perso-6.2.diff
```

Replace this hunk:
```console
@@ -103,7 +103,9 @@ static Button buttons[] = {
 	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
 	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
 	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
-	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
+	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
+	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
+	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
 	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
 	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
 	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
```

By this hunk:
```console
@@ -118,7 +118,9 @@ static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
-	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
+	{ ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
+	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
+	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
```

And replace this hunk:
```console
@@ -439,9 +445,26 @@ buttonpress(XEvent *e)
 			arg.ui = 1 << i;
 		} else if (ev->x < x + blw)
 			click = ClkLtSymbol;
-		else if (ev->x > selmon->ww - TEXTW(stext))
+		else if (ev->x > (x = selmon->ww - TEXTW(stext) + lrpad)) {
 			click = ClkStatusText;
-		else
+
+			char *text = rawstext;
+			int i = -1;
+			char ch;
+			dwmblockssig = 0;
+			while (text[++i]) {
+				if ((unsigned char)text[i] < ' ') {
+					ch = text[i];
+					text[i] = '\0';
+					x += TEXTW(text) - lrpad;
+					text[i] = ch;
+					text += i+1;
+					i = -1;
+					if (x >= ev->x) break;
+					dwmblockssig = ch;
+				}
+			}
+		} else
 			click = ClkWinTitle;
 	} else if ((c = wintoclient(ev->window))) {
 		focus(c);
```

By this hunk:
```console
@@ -529,8 +529,25 @@ buttonpress(XEvent *e)
			arg.ui = 1 << i;
		} else if (ev->x < x + blw)
			click = ClkLtSymbol;
-		else
+		else if (ev->x > (x = selmon->ww - TEXTW(stext) + lrpad)) {
			click = ClkStatusText;
+			char *text = rawstext;
+			int i = -1;
+			char ch;
+			dwmblockssig = 0;
+			while (text[++i]) {
+				if ((unsigned char)text[i] < ' ') {
+					ch = text[i];
+					text[i] = '\0';
+					x += TEXTW(text) - lrpad;
+					text[i] = ch;
+					text += i+1;
+					i = -1;
+					if (x >= ev->x) break;
+					dwmblockssig = ch;
+				}
+			}
+		}
	} else if ((c = wintoclient(ev->window))) {
		focus(c);
		restack(selmon);
```

```console
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-statuscmd-signal-perso-6.2.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

* Apply the [`swallow`](https://dwm.suckless.org/patches/swallow/) patch:

```console
$ cd /path/to/this/repo/patches
$ curl https://dwm.suckless.org/patches/swallow/dwm-swallow-20200807-b2de9b0.diff > dwm-swallow-20200807-b2de9b0.diff
```

For the patch to work with all the previous patches, you will have to modify it.
```console
$ cp dwm-swallow-20200807-b2de9b0.diff dwm-swallow-perso-20200807-b2de9b0.diff
$ vi dwm-swallow-perso-20200807-b2de9b0.diff
```

Replace this hunk:
```console
@@ -92,9 +98,11 @@ struct Client {
 	int basew, baseh, incw, inch, maxw, maxh, minw, minh;
 	int bw, oldbw;
 	unsigned int tags;
-	int isfixed, isfloating, isurgent, neverfocus, oldstate, isfullscreen;
+	int isfixed, isfloating, isurgent, neverfocus, oldstate, isfullscreen, isterminal, noswallow;
+	pid_t pid;
 	Client *next;
 	Client *snext;
+	Client *swallowing;
 	Monitor *mon;
 	Window win;
 };
@@ -138,6 +146,8 @@ typedef struct {
 	const char *title;
 	unsigned int tags;
 	int isfloating;
+	int isterminal;
+	int noswallow;
 	int monitor;
 } Rule;
 
```

By this hunk:
```console
@@ -98,9 +104,11 @@ struct Client {
 	int basew, baseh, incw, inch, maxw, maxh, minw, minh;
 	int bw, oldbw;
 	unsigned int tags;
-	int isfixed, isfloating, isurgent, neverfocus, oldstate, isfullscreen, issticky;
+	int isfixed, isfloating, isurgent, neverfocus, oldstate, isfullscreen, issticky, isterminal, noswallow;
+	pid_t pid;
 	Client *next;
 	Client *snext;
+	Client *swallowing;
 	Monitor *mon;
 	Window win;
 };

```

And replace this hunk:
```console
@@ -19,10 +19,11 @@ FREETYPELIBS = -lfontconfig -lXft
 FREETYPEINC = /usr/include/freetype2
 # OpenBSD (uncomment)
 #FREETYPEINC = ${X11INC}/freetype2
+#KVMLIB = -lkvm
 
 # includes and libs
 INCS = -I${X11INC} -I${FREETYPEINC}
-LIBS = -L${X11LIB} -lX11 ${XINERAMALIBS} ${FREETYPELIBS}
+LIBS = -L${X11LIB} -lX11 ${XINERAMALIBS} ${FREETYPELIBS} -lX11-xcb -lxcb -lxcb-res ${KVMLIB}
 
 # flags
 CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_POSIX_C_SOURCE=200809L -DVERSION=\"${VERSION}\" ${XINERAMAFLAGS}
```

By this hunk:
```console
@@ -19,10 +19,11 @@ FREETYPELIBS = -lfontconfig -lXft
 FREETYPEINC = /usr/include/freetype2
 # OpenBSD (uncomment)
 #FREETYPEINC = ${X11INC}/freetype2
+#KVMLIB = -lkvm
 
 # includes and libs
 INCS = -I${X11INC} -I${FREETYPEINC}
-LIBS = -L${X11LIB} -lX11 ${XINERAMALIBS} ${FREETYPELIBS} -lXrender
+LIBS = -L${X11LIB} -lX11 ${XINERAMALIBS} ${FREETYPELIBS} -lXrender -lX11-xcb -lxcb -lxcb-res ${KVMLIB}
 
 # flags
 CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_POSIX_C_SOURCE=200809L -DVERSION=\"${VERSION}\" ${XINERAMAFLAGS}
```

```console
$ cd ../dwm-x.x
$ patch -p1 < ../patches/dwm-swallow-perso-20200807-b2de9b0.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

> Note that you will have to run `xprop` and select your default terminal (e.g. `st`) in order to
> get it's class name (e.g. for `st` the class name can differ: `St`, `st`, `st-25color`...). Then
> you will have to change the class name accordingly in your `dwm`'s `config.h`, e.g.:
> `{ "st-256color", NULL,    NULL,           0,        0,         1,           0,       -1 },`

