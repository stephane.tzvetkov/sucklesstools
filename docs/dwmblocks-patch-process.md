# dwmblocks patch process

Here is the full patch process I applied to `dwmblocks`:

> Note that for the following patch to work seamlessly, `dwmblocks` needs to be checked out to the
> commit `b12722a` (`$ git checkout b12722a`), but you might also get it working on other commits
> with little extra effort.

* Apply the [`dwmblocks-statuscmd`](https://github.com/torrinfail/dwmblocks#patches) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://gist.githubusercontent.com/toniz4/41d168719e22bf7bc4ecff09d424b7d2/raw/ce9275826f8b6f1355dca11a3f5f157cfeba6702/dwmblocks-statuscmd-20200717-941f415.diff > dwmblocks-statuscmd-20200717-941f415.diff

$ cd ../dwmblocks-b12722a
$ patch -p1 < ../patches/dwmblocks-statuscmd-20200717-941f415.diff
$ make clean && make
```

> Note that for this patch to work: you will also need `dwm` to be patched (cf. [dwmblocks
> dependencies](../README.md#dwmblocks-dependencies), and [dwm patch
> process](./dwm-patch-process.md) for more details).

