# slock patch process

Here is the full patch process I applied to `slock`:

* Apply the [`blur pixelated
  screen`](https://tools.suckless.org/slock/patches/blur-pixelated-screen/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://tools.suckless.org/slock/patches/blur-pixelated-screen/slock-blur_pixelated_screen-1.4.diff > slock-blur_pixelated_screen-1.4.diff
```

For the patch to work, you will have to modify it.
```console
$ cp slock-blur_pixelated_screen-1.4.diff slock-blur_pixelated_screen-perso-1.4.diff
$ vi slock-blur_pixelated_screen-perso-1.4.diff
```

Replace this part:
```console
@@ -16,7 +16,7 @@ LIBS = -L/usr/lib -lc -lcrypt -L${X11LIB} -lX11 -lXext -lXrandr -lImlib2
 
 # flags
 CPPFLAGS = -DVERSION=\"${VERSION}\" -D_DEFAULT_SOURCE -DHAVE_SHADOW_H
-CFLAGS = -std=c99 -pedantic -Wall -Os ${INCS} ${CPPFLAGS}
+CFLAGS = -std=c99 -pedantic -Wall -Ofast ${INCS} ${CPPFLAGS}
 LDFLAGS = -s ${LIBS}
 COMPATSRC = explicit_bzero.c
```

By this part:
```console
@@ -16,7 +16,7 @@ LIBS = -L/usr/lib -lc -lcrypt -L${X11LIB} -lX11 -lXext -lXrandr -lImlib2
 
 # flags
 CPPFLAGS += -DVERSION=\"${VERSION}\" -D_DEFAULT_SOURCE -DHAVE_SHADOW_H
-CFLAGS += -std=c99 -pedantic -Wall -Os ${INCS} ${CPPFLAGS}
+CFLAGS += -std=c99 -pedantic -Wall -Ofast ${INCS} ${CPPFLAGS}
 LDFLAGS += -s ${LIBS}
 COMPATSRC += explicit_bzero.c
```

Then:
```console
$ cd ../slock-x.x
$ patch -p1 < ../patches/slock-blur_pixelated_screen-perso-1.4.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && sudo make install
```

* Apply the [`dpms`](https://tools.suckless.org/slock/patches/dpms) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://tools.suckless.org/slock/patches/dpms/slock-dpms-1.4.diff > slock-dpms-1.4.diff
```

For the patch to work, you will have to modify it.
```console
$ cp slock-dpms-1.4.diff slock-dpms-perso-1.4.diff
$ vi slock-dpms-perso-1.4.diff
```

Replace this part:
```console
@@ -10,3 +10,6 @@ static const char *colorname[NUMCOLS] = {
 
 /* treat a cleared input like a wrong password (color) */
 static const int failonclear = 1;
+
+/* time in seconds before the monitor shuts down */
+static const int monitortime = 5;
```

By this part:
```console
@@ -11,6 +11,9 @@ static const char *colorname[NUMCOLS] = {
 /* treat a cleared input like a wrong password (color) */
 static const int failonclear = 1;

+/* time in seconds before the monitor shuts down */
+static const int monitortime = 5;
+
 /*Enable blur*/
 #define BLUR
 /*Set blur radius*/
```

Then:
```console
$ cd ../slock-x.x
$ patch -p1 < ../patches/slock-dpms-perso-1.4.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && sudo make install
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

