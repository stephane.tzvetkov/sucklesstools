# st patch process

Here is the full patch process I applied to `st`:

* Apply the [`alpha`](https://st.suckless.org/patches/alpha/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://st.suckless.org/patches/alpha/st-alpha-0.8.2.diff > st-alpha-0.8.2.diff
$ cd ../st-x.x.x
$ patch -p1 < ../patches/st-alpha-0.8.2.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the [`scrollback`](https://st.suckless.org/patches/scrollback/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://st.suckless.org/patches/scrollback/st-scrollback-20200419-72e3f6c.diff > st-scrollback-20200419-72e3f6c.diff
$ cd ../st-x.x.x
$ patch -p1 < ../patches/st-scrollback-20200419-72e3f6c.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the [`ligatures`](https://st.suckless.org/patches/ligatures/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://st.suckless.org/patches/ligatures/0.8.3/st-ligatures-alpha-scrollback-20200430-0.8.3.diff > st-ligatures-alpha-scrollback-20200430-0.8.3.diff
$ cd ../st-x.x.x
$ patch -p1 ../patches/st-ligatures-alpha-scrollback-20200430-0.8.3.diff
$ make clean && make
```

* Apply the [`keyboard_select`](https://st.suckless.org/patches/keyboard_select/) patch ("by
  hand"):
  ```console
  $ cd /path/to/this/repo/patches
  $ curl https://st.suckless.org/patches/keyboard_select/st-keyboard_select-0.8.2.diff > st-keyboard_select-0.8.2.diff
  $ cd ../st-x.x.x
  ```

  Now one might notice that using `patch` to apply the `keyboard-select` patch is not possible
  after all the previous patches. In this case, pathing "by hand" is the only solution: open the
  `st-keyboard_select-0.8.2.diff` patch in one editor and apply the diff line by line in an
  other one.

  **With the following difference...**
  In `st-st-keyboard_select-0.8.2.diff`, Replace:<br />
  ```console
        xdrawcursor(term.c.x, term.c.y, term.line[term.c.y][term.c.x],
                    term.ocx, term.ocy, term.line[term.ocy][term.ocx]);
  ```
  By:
  ```console
        xdrawcursor(term.c.x, term.c.y, term.line[term.c.y][term.c.x],
                term.ocx, term.ocy, term.line[term.ocy][term.ocx],
                term.line[term.ocy], term.col);
  ```

  Then
  ```console
  $ make clean && make
  ```

* Apply the [`font2`](https://st.suckless.org/patches/font2/) patch ("by hand"):
  ```console
  $ cd /path/to/this/repo/patches
  $ curl https://st.suckless.org/patches/font2/st-font2-20190416-ba72400.diff > st-font2-20190416-ba72400.diff
  $ cd ../st-x.x.x
  ```

  Now one might notice that using `patch` to apply the `font2` patch is not possible after all the
  previous patches. In this case, pathing "by hand" is the only solution: open the
  `st-font2-20190416-ba72400.diff` patch in one editor and apply the diff line by line in an
  other one.

  Then:
  ```console
  $ make clean && make
  ```
  > Note that you will get compilation error here until you merge the patched `config.def.h` diff
    with `config.h`.

* Apply the [`vertcenter`](https://st.suckless.org/patches/vertcenter/) patch ("by hand"):
  ```console
  $ cd /path/to/this/repo/patches
  $ curl https://st.suckless.org/patches/vertcenter/st-vertcenter-20180320-6ac8c8a.diff > st-vertcenter-20180320-6ac8c8a.diff
  $ cd ../st-x.x.x
  ```

  Now one might notice that using `patch` to apply the `vertcenter` patch is not possible after all
  the previous patches. In this case, pathing "by hand" is the only solution: open the
  `st-vertcenter-20180320-6ac8c8a.diff` patch in one editor and apply the diff line by line in an
  other one.

  Then:
  ```console
  $ make clean && make
  ```

* Apply the [`anysize`](https://st.suckless.org/patches/anysize/) patch ("by hand"):
  ```console
  $ cd /path/to/this/repo/patches
  $ curl https://st.suckless.org/patches/anysize/st-anysize-0.8.1.diff > st-anysize-0.8.1.diff
  $ cd ../st-x.x.x
  ```

  Now one might notice that using `patch` to apply the `vertcenter` patch is not possible after all
  the previous patches. In this case, pathing "by hand" is the only solution: open the
  `st-anysize-0.8.1.diff` patch in one editor and apply the diff line by line in an
  other one.

  Then:
  ```console
  $ make clean && make
  ```

* Apply the [`st-newterm`](https://st.suckless.org/patches/newterm/st-newterm-0.8.2.diff) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://st.suckless.org/patches/newterm/st-newterm-0.8.2.diff > st-newterm-0.8.2.diff
```

For the patch to work with all the previous patches, you will have to modify it.
```console
$ cp st-newterm-0.8.2.diff st-newterm-perso-0.8.2.diff
$ vi st-newterm-perso-0.8.2.diff
```

Replace this hunk:
```console
@@ -80,6 +80,7 @@ void die(const char *, ...);
 void redraw(void);
 void draw(void);
 
+void newterm(const Arg *);
 void printscreen(const Arg *);
 void printsel(const Arg *);
 void sendbreak(const Arg *);
```

By this hunk:
```console
@@ -80,6 +80,7 @@ void die(const char *, ...);
 void redraw(void);
 void draw(void);
 
+void newterm(const Arg *);
 void kscrolldown(const Arg *);
 void kscrollup(const Arg *);
 void printscreen(const Arg *);
```

And replace this hunk:
```console
@@ -178,6 +178,7 @@ static Shortcut shortcuts[] = {
 	{ TERMMOD,              XK_Y,           selpaste,       {.i =  0} },
 	{ ShiftMask,            XK_Insert,      selpaste,       {.i =  0} },
 	{ TERMMOD,              XK_Num_Lock,    numlock,        {.i =  0} },
+	{ TERMMOD,              XK_Return,      newterm,        {.i =  0} },
 };
 
 /*
```

By this hunk:
```console
@@ -178,6 +178,7 @@ static Shortcut shortcuts[] = {
 	{ TERMMOD,              XK_Y,           selpaste,       {.i =  0} },
 	{ ShiftMask,            XK_Insert,      selpaste,       {.i =  0} },
 	{ TERMMOD,              XK_Num_Lock,    numlock,        {.i =  0} },
+	{ TERMMOD,              XK_Return,      newterm,        {.i =  0} },
	{ ShiftMask,            XK_Page_Up,     kscrollup,      {.i = -1} },
	{ ShiftMask,            XK_Page_Down,   kscrolldown,    {.i = -1} },
    { TERMMOD,              XK_Escape,      keyboard_select,{      0} },
```

Then:
```console
$ cd ../st-x.x.x
$ patch -p1 < ../patches/st-newterm-0.8.2.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the [`iso14755`](https://st.suckless.org/patches/iso14755/) patch:

> Note that for this patch to work with `dmenu`, you will need to patch `dmenu` with the `alpha`
> patch (cf. [dmenu patch process](./dmenu-patch-process.md) for more details).

```console
$ cd /path/to/this/repo/patches
$ curl https://st.suckless.org/patches/iso14755/st-iso14755-0.8.3.diff > st-iso14755-0.8.3.diff
```

For the patch to work with all the previous patches, you will have to modify it.
```console
$ cp st-iso14755-0.8.3.diff st-iso14755-perso-0.8.3.diff
$ vi st-iso14755-perso-0.8.3.diff
```

Replace this hunk:
```console
@@ -193,6 +198,7 @@ static Shortcut shortcuts[] = {
 	{ TERMMOD,              XK_Y,           selpaste,       {.i =  0} },
 	{ ShiftMask,            XK_Insert,      selpaste,       {.i =  0} },
 	{ TERMMOD,              XK_Num_Lock,    numlock,        {.i =  0} },
+	{ TERMMOD,              XK_I,           iso14755,       {.i =  0} },
 };
 
 /*
```

By this hunk:
```console
@@ -188,6 +193,7 @@ static Shortcut shortcuts[] = {
 	{ TERMMOD,              XK_Y,           selpaste,       {.i =  0} },
 	{ ShiftMask,            XK_Insert,      selpaste,       {.i =  0} },
 	{ TERMMOD,              XK_Num_Lock,    numlock,        {.i =  0} },
+	{ TERMMOD,              XK_I,           iso14755,       {.i =  0} },
	{ TERMMOD,              XK_Return,      newterm,        {.i =  0} },
	{ ShiftMask,            XK_Page_Up,     kscrollup,      {.i = -1} },
	{ ShiftMask,            XK_Page_Down,   kscrolldown,    {.i = -1} },
```

And replace those two hunks:
```console
@@ -81,6 +81,7 @@ void die(const char *, ...);
 void redraw(void);
 void draw(void);
 
+void iso14755(const Arg *);
 void printscreen(const Arg *);
 void printsel(const Arg *);
 void sendbreak(const Arg *);
@@ -122,3 +123,4 @@ extern char *termname;
 extern unsigned int tabspaces;
 extern unsigned int defaultfg;
 extern unsigned int defaultbg;
+extern char *iso14755_cmd;
```

By those hunks:
```console
@@ -81,6 +81,7 @@ void die(const char *, ...);
 void redraw(void);
 void draw(void);
 
+void iso14755(const Arg *);
 void newterm(const Arg *);
 void kscrolldown(const Arg *);
 void kscrollup(const Arg *);
@@ -122,3 +123,4 @@ extern char *termname;
 extern unsigned int defaultfg;
 extern unsigned int defaultbg;
 extern float alpha;
+extern char *iso14755_cmd;
```

Then:
```console
$ cd ../st-x.x.x
$ patch -p1 < ../patches/st-iso14755-perso-0.8.3.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```
> Note that you will get compilation error here until you merge the patched `config.def.h` diff
  with `config.h`.

* Apply the [`st-scrollback-mouse`](https://st.suckless.org/patches/scrollback/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://st.suckless.org/patches/scrollback/st-scrollback-mouse-20191024-a2c479c.diff > st-scrollback-mouse-20191024-a2c479c.diff
$ cd ../st-x.x.x
$ patch -p1 < ../patches/st-scrollback-mouse-20191024-a2c479c.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

* Apply the [`st-scrollback-mouse-altscreen`](https://st.suckless.org/patches/scrollback/) patch:
```console
$ cd /path/to/this/repo/patches
$ curl https://st.suckless.org/patches/scrollback/st-scrollback-mouse-altscreen-20200416-5703aa0.diff > st-scrollback-mouse-altscreen-20200416-5703aa0.diff
$ cd ../st-x.x.x
$ patch -p1 < ../patches/st-scrollback-mouse-altscreen-20200416-5703aa0.diff
$ diff config.h config.def.h # don't forget to merge what you see here into your config.h
$ make clean && make
```

