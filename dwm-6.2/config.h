/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const int gappx              = 0;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {
    "NotoColorEmoji:pixelsize=16:antialias=true:autohint=true",
    "Hack Nerd Font:pixelsize=16:antialias=true:autohint=true",
    "Symbola:pixelsize=16:antialias=true:autohint=true",
    "Inconsolata:pixelsize=16:antialias=true:autohint=true",
};
static const char dmenufont[]       = "Hack Nerd Font:pixelsize=16:antialias=true:autohint=true";

/* colors */
static const char col_gray1[]        = "#222222";
static const char col_gray2[]        = "#444444";
static const char col_gray3[]        = "#bbbbbb";
static const char col_gray4[]        = "#eeeeee";

static const char col_white[]        = "#ffffff";
static const char col_black[]        = "#000000";

static const char col_red[]          = "#ff0000"; /* x11 red */
static const char col_orangered[]    = "#ff5f00"; /* x11 orange red 1 */
static const char col_orange[]       = "#ffa500"; /* x11 orange */
static const char col_gold[]         = "#ffd700"; /* x11 gold */
static const char col_yellow[]       = "#ffff00"; /* x11 yellow */
static const char col_greenyellow[]  = "#adff2f"; /* x11 green yellow */
static const char col_limegreen[]    = "#32cd32"; /* x11 lime green */
static const char col_springgreen[]  = "#00ff7f"; /* x11 spring green */
static const char col_turquoise[]    = "#40e0d0"; /* x11 turquoise */
static const char col_cyan[]         = "#00FFFF"; /* x11 cyan */
static const char col_deepskyblue[]  = "#00BFFF"; /* x11 deep sky blue */
static const char col_royalblue[]    = "#4169e1"; /* x11 royal blue */
static const char col_navyblue[]     = "#000080"; /* x11 navy blue */
static const char col_midnightblue[] = "#191970"; /* x11 midnight blue */
static const char col_blueviolet[]   = "#8a2be2"; /* x11 blue violet */
static const char col_violetred[]    = "#ee3a8c"; /* x11 violet red */
static const char col_magenta[]      = "#ff00ff"; /* x11 magenta */
static const char col_hotpink[]      = "#ff69b4"; /* x11 hot pink */
static const char col_ceared[]       = "#ba0c1a"; /* CEA red */

static const unsigned int baralpha = 204; // hex value (e.g. 0xFF) or dec value between 0 and 255
                                          // here 204 is 80% of 255
static const unsigned int borderalpha = OPAQUE; // OPAQUE is defined to be 0xFF, same as 255
static const char *colors[][3]      = {
    /*               fg         bg         border   */
    //[SchemeNorm] = { col_gray2, col_black, col_gray2 }, // old variant
    //[SchemeNorm] = { col_royalblue, col_black, col_gray2 }, // blue variant
    //[SchemeNorm] = { col_ceared, col_black, col_gray2 }, // cea red variant
    //[SchemeNorm] = { col_white, col_black, col_gray2 }, // contrast variant
    [SchemeNorm] = { col_gray2, col_black, col_gray2 }, // gray variant

    //[SchemeSel]  = { col_white, col_black,  col_white  }, // old variant
    //[SchemeSel]  = { col_black, col_white,  col_white  }, // contrast variant
    [SchemeSel]  = { col_gold, col_black,  col_gold  }, // gold variant
};
static const unsigned int alphas[][3]      = {
    /*               fg      bg        border     */
    [SchemeNorm] = { OPAQUE, baralpha, borderalpha },
    //[SchemeNorm] = { baralpha, baralpha, borderalpha },
    [SchemeSel]  = { OPAQUE, baralpha, borderalpha },
    //[SchemeSel]  = { baralpha, baralpha, borderalpha },
};

/* tagging */
//static const char *tags[] = { " ¹", " ²", " ³", " ⁴", " ⁵", " ⁶", " ⁷", " ⁸", " ⁹" };
static const char *tags[] = { "¹", "²", "³", "⁴", "⁵", "⁶", "⁷", "⁸", "⁹" };
//   
//   
//  
//                    
//      

static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */
	/* class         instance title           tags mask isfloating isterminal  noswallow monitor */
	{ "Firefox",     NULL,    NULL,           1 << 8,   0,         0,          -1,       -1 },
	{ "st-256color", NULL,    NULL,           0,        0,         1,           0,       -1 },
	{ NULL,          NULL,    "Event Tester", 0,        0,         0,           1,       -1 },//xev
};

/* layout(s) */
static const float mfact     = 0.5;   /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;     /* number of clients in master area */
static const int resizehints = 1;     /* 1 means respect size hints in tiled resizals */
static const int attachdirection = 2; /* 0 default, 1 above, 2 aside, 3 below, 4 bottom, 5 top */

static const Layout layouts[] = {
    /* symbol     arrange function */
    //{ "[]=",      tile },    /* first entry is default */
    { " ﬿",      tile },    /* first entry is default */
    //{ "><>",      NULL },    /* no layout function means floating behavior */
    { " ",      NULL },    /* no layout function means floating behavior */
    //{ "[M]",      monocle },
    { " ",      monocle },
};
//  
// 类         侀ﰧ 充冀ﰦ 全况﩯﩮  舘𤋮恵頻 

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
//static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *slockcmd[]  = { "slock", NULL };

#include "movestack.c"
#include "shiftview.c"

static Key keys[] = {
    /* modifier                     key        function        argument */
    { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
    { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
    { MODKEY,                       XK_Escape, spawn,          {.v = slockcmd } },
    { MODKEY,                       XK_b,      togglebar,      {0} },
    { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
    { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
    { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
    { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
    { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
    { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
    { MODKEY,                       XK_Return, zoom,           {0} },
//  { MODKEY,                       XK_Tab,    view,           {0} },
    { MODKEY,                       0xb2,      view,           {0} }, /* ² */
    { MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
    { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
    { MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
    { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
    { MODKEY,                       XK_space,  setlayout,      {0} },
    { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
    { MODKEY,                       XK_s,      togglesticky,   {0} },
    { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
    { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
    { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
    { MODKEY,                       XK_period, focusmon,       {.i = +1 } },
    { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
    { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
    { MODKEY,                       XK_minus,  setgaps,        {.i = -5 } },
    { MODKEY,                       XK_equal,  setgaps,        {.i = +5 } },
    { MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
    { MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
    { MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
    { MODKEY,                       XK_Tab,    shiftview,      { .i = +1 } },
    { MODKEY|ShiftMask,             XK_Tab,    shiftview,      { .i = -1 } },
    TAGKEYS(                        0x26,                      0) /* & */
    TAGKEYS(                        0xe9,                      1) /* é */
    TAGKEYS(                        0x22,                      2) /* " */
    TAGKEYS(                        0x27,                      3) /* ' */
    TAGKEYS(                        0x28,                      4) /* ( */
    TAGKEYS(                        0x2d,                      5) /* - */
    TAGKEYS(                        0xe8,                      6) /* è */
    TAGKEYS(                        0x5f,                      7) /* _ */
    TAGKEYS(                        0xe7,                      8) /* ç */
    TAGKEYS(                        0xe0,                      9) /* à */
    { MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkStatusText,        0,              Button1,        sigdwmblocks,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigdwmblocks,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigdwmblocks,   {.i = 3} },
    { ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
    { ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
    { ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,              Button1,        view,           {0} },
    { ClkTagBar,            0,              Button3,        toggleview,     {0} },
    { ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
    { ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

