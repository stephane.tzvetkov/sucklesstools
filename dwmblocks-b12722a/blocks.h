// Modify this file to change what commands output to your statusbar,
// and recompile using the make command.

// Use `pkill -RTMIN+42 dwmblocks` to send an update signal (replace 42 by the signal nb you want).

static const Block blocks[] = {
	/*Icon*/    /*Command*/                  /*Update Interval*/     /*Update Signal*/
    {"",	    "dwmblocks-kernel",          86400,                  23},
    {"",	    "dwmblocks-last-update",     86400,                  22},
    //{"",	    "dwmblocks-nb-packages",     86400,                  21},
    {"",	    "dwmblocks-cpu-bars",        1,                      20},
    {"",	    "dwmblocks-cpu-temp",        10,                     18},
    {"",        "dwmblocks-memory",          10,                     14},
    {"",        "dwmblocks-disk-space",      60,                     12},
    {"",        "dwmblocks-net-traffic",     1,                      10},
    {"",        "dwmblocks-internet",        1,                      8},
    {"",        "dwmblocks-ip-locate",       10,                     7},
    //{"",      "dwmblocks-weather-now",     300,                    6},
    {"",        "dwmblocks-weather-predict", 18000,                  5},
    {"",	    "dwmblocks-redshift-temp",	 1,	                     4},
    {"",	    "dwmblocks-brightness",	     1,	                     3},
    {"",        "dwmblocks-battery",         1,                      2},
    {"",	    "dwmblocks-clock",	         60,	                 1},
};

// sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "  ";
static unsigned int delimLen = 4; // 5 for a less compact bar
